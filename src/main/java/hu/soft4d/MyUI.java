package hu.soft4d;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import java.io.File;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        //mainLayout
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        //headerLayout
        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setHeight("100px");

        Label headerLabel = new Label("Header");
        headerLayout.addComponent(headerLabel);
        headerLayout.setComponentAlignment(headerLabel, Alignment.MIDDLE_LEFT);

        Label testLabel = new Label("Test");
        headerLayout.addComponent(testLabel);
        headerLayout.setComponentAlignment(testLabel, Alignment.MIDDLE_LEFT);

        Label sampleLabel = new Label("Sample");
        headerLayout.addComponent(sampleLabel);
        headerLayout.setComponentAlignment(sampleLabel, Alignment.MIDDLE_LEFT);

        layout.addComponent(headerLayout);
        layout.setComponentAlignment(headerLayout, Alignment.TOP_LEFT);

        //middleLayout
        HorizontalLayout middleLayout = new HorizontalLayout();
        middleLayout.setSizeUndefined();
        middleLayout.setHeight("100%");
        middleLayout.setWidth("100%");

        //listLayout in middleLayout
        VerticalLayout listLayout = new VerticalLayout();
        Table table = new Table();
        table.setSizeFull();
        table.addContainerProperty("Name", String.class, null);
        Map<String, String> env = System.getenv();

        /*int i = 1;
        for (Map.Entry<String, String> entry : env.entrySet()) {
            table.addItem(new String[]{entry.getKey()}, i);
            i++;
        }*/
        //table.setContainerDataSource; ???
        // ki kell javítani hogy ne legyen benne for ciklus hanem a datasource segítségével legyen kilistázva (a map??)

        BeanItemContainer<Map.Entry<String, String>> envContainer = new BeanItemContainer<Map.Entry<String, String>>(Map.Entry.class, env.entrySet());
        table.setContainerDataSource(envContainer);
        table.setVisibleColumns("key");

        table.setSelectable(true);
        table.setImmediate(true);

        listLayout.addComponent(table);
        listLayout.setSizeFull();
        listLayout.setWidth("250px");

        //contentLayout in middleLayout
        VerticalLayout contentLayout = new VerticalLayout();
        TextArea contentTextArea = new TextArea();
        contentTextArea.setSizeFull();

        contentLayout.addComponent(contentTextArea);

        contentLayout.setSizeFull();

        table.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                contentTextArea.setReadOnly(false);
                if(table.getValue() != null){
                    String[] value = table.getContainerProperty(event.getProperty().getValue(), "value").getValue().toString().split(File.pathSeparator);
                            //env.get(table.getItem(table.getValue()).toString()).split(File.pathSeparator);
                    String delimitedValue = "";
                    for(int i=0; i<value.length; i++){
                        delimitedValue += value[i] + "\n";
                    }

                    contentTextArea.setValue(delimitedValue);
                }else{
                    contentTextArea.setValue("");
                }

                contentTextArea.setReadOnly(true);
            }
        });

        middleLayout.addComponent(listLayout);
        middleLayout.addComponent(contentLayout);
        middleLayout.setExpandRatio(contentLayout, 1);
        layout.addComponent(middleLayout);

        layout.setExpandRatio(middleLayout, 1);

        //footerLayout
        VerticalLayout footerLayout = new VerticalLayout();
        Label footerLabel = new Label("Footer");

        footerLayout.addComponent(footerLabel);
        footerLayout.setSizeUndefined();
        footerLayout.setHeight("100px");

        layout.addComponent(footerLayout);
        layout.setComponentAlignment(footerLayout, Alignment.BOTTOM_CENTER);
        footerLayout.setComponentAlignment(footerLabel, Alignment.BOTTOM_CENTER);

        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
